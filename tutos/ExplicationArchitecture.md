# Explication de l'architecture d'un projet Expo

## _Les fichiers à la racine_

### **babel.config.js**
Babel est un compilateur qui assure une stabilité des versions de Javascript.
Il va permettre d'utiliser du JSX, une version plus récente du JS qui permet d'utiliser des syntax optimisées, ce qui nous fait gagner du temps.
En résumé, babel permet de traduire du JS nouvelle version en ancienne version pour pouvoir compiler.

### ***package.json***
Ce fichier permet de stocker toutes les références des dépendances utilisées dans le projet. 
Lorsqu'on utilise la commande 
```
npm install quelquechose
```
La dépendance s'ajoute dans le fichier

Pour faire une vérification des dépendances installées et ajouter celles manquantes, il faut faire : 
```
npm install
```
Cependant vous aller voir par la suite que ce n'est pas la meilleure des méthodes

### ***package-lock.json***
Ce fichier permet lui aussi de stocker les références de dépendances. Cependant ce dernier est beaucoup plus précis. Chaque version est clairement définie, ce qui empêche le conflit de version entre chaque dépendances. 

Par exemple, si aujourd'hui je lance la commande npm install, je n'aurai peut être pas le même résultat demain car la version de la dépendance peut changer entre temps 

Pour pouvoir vérifier et mettre à jour les dépendances de notre projet à la version exacte, on utilise la commande : 
```
npm ci
```

**On utilisera tout le temps cette commande si un problème de dépendances a lieu !**

Le fichier yarn.lock a la même utilité.

### ***App.js***
Le point d'entrée de notre application. C'est la première page lue par expo, et donc la première page sur laquelle l'utilisateur tombe lors du démarrage de l'application

### ***app.json***
Pas mal de configuration possible sur ce fichier, faut voir ce qu'on peut faire avec.
On peut changer l'image de chargement d'expo ici, donc ça peut être marrant d'essayer ;)

### ***gitignore***
C'est un fichier qui permet d'exclure certains fichier/dossier de notre repo git, ça évite de trop polluer et de ne se concentrer que sur le nécessaire

## _Passons aux dossiers maintenant !_

### ***.expo***
Un dossier créé lors du premier lancement de l'application. 

C'est un dossier uniquement local, donc individuel. Il y a un read me dans ce dossier, vous pourrez y faire un tour.

### ***.expo-shared***
Un dossier qui, comme son nom l'indique, doit être partagé.

Il permet d'optimiser le travail à plusieurs (notamment pour une optimisation des prises en charge des images je crois, mais bon j'ai pas trouvé beaucoup d'info sur ça)

***Envoyez un message sur le discord avec le mot topinambour*** 

(j'avais pas d'idée de mot ok). ça peut être une phrase ou juste le mot, vous pouvez même me l'envoyer en MP ou n'importe ou sur le discord.

C'est pour voir qui à le courage de lire jusqu'au bout ;)

### ***assets***
Aucune réelle utilitée technique. 

Ce dossier est là pour de l'organisation, c'est ici que nous allons stocker les images locales de notre application

### ***components***
Un dossier que j'ai créé pour faciliter l'organisation

On va pouvoir mettre ici les composants que nous développons

On va aussi les découper encore en sous dossier je pense (ex : un components/gestionUtilisateur/..)

### ***node_modules***

Très très gros dossier, il fait plus de 300 MO. C'est lui qui contient tout le code source de nos dépendances, des composants natifs, le code source d'expo...

C'est pour ça qu'on ne le push pas sur git, c'est beaucoup trop lourd.

Il ne faut pas toucher à ce dossier, à moins que l'on veuille modifier une dépendance directement (ce qui, à notre niveau n'est vraiment pas nécessaire).

### ***styles***
On va mettre ici les styles globaux de notre application.

Par exemple, le fichier styles/Text.js va définir un style pour du texte de couleur rouge, un style pour du texte en font size 11...

ça va éviter de faire de la répétition dans la déclaration de style, parce que sinon ça peut vite venir alourdir notre code.

Le fichier styles/index.js permet de centraliser les styles et de ne faire appel qu'au dossier "styles" lors d'un import.

### ***tutos***
C'est mon terrain de jeu :) 

On va pouvoir mettre des tutos un peu personnalisés ici, des infos sur une dépendance...

Globalement tout ce qui peut être utile pour la compréhension du code

### ***utils***
On va mettre ici toutes les méthodes/fonctions qu'on appelle régulièrement, afin de ne pas avoir à les réécrire à chaque fois 

Pour l'instant rien ici, mais ça arrive.

### ***test unitaires*** 
Il faut que je me documente sur ça, donc ça va arriver bientot ! 