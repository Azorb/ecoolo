# Règles de développement

## Développement pur


### Convention de nommage

**Variables** : 

Explicite et pas trop long, toujours en **anglais**

On l'écrit en **camelCase** 

ex : `dateDebut`

**Fonctions** : 

Pareil que les variables. 

**Les fichiers et dossiers**

Nom des fichiers et dossiers en minuscule 

En anglais aussi

ex : `subscribe.js`

### **Commentaires**

On commente : 
- les fonctions
- les tests 
- les composants

Pour les fonctions : 

- Description de l'objectif de la fonction
- Description des arguments quand c'est nécessaire
- Valeur retournée

Si vous faites un code qui vous paraît compliqué à la lecture, commentez le. 

N'expliquez pas chaque ligne !!

Pour les tests : 

- Quel type de test : (à completer)
- Description de l'objectif du test
- Description des arguments du test

Pour les composants : 

- Description de leur utilité


## Règles gestion du git

Il faut un git propre !! 

Soyez explicite sur les noms des commits

La forme d'un commit : 

`type : sujet`


- build : changements qui affectent le système de build ou des dépendances externes (npm, make…)

- ci : changements concernant les fichiers et scripts d’intégration ou de configuration (Travis, Ansible, BrowserStack…)

- feat : ajout d’une nouvelle fonctionnalité

- fix : correction d’un bug

- perf : amélioration des performances

- refactor : modification qui n’apporte ni nouvelle fonctionalité ni d’amélioration de performances

- style : changement qui n’apporte aucune alteration fonctionnelle ou sémantique (indentation, mise en forme, ajout d’espace, renommante d’une variable…)

- docs : rédaction ou mise à jour de documentation

- test : ajout ou modification de tests

- revert. Ce dernier permet comme son nom l’indique, d’annuler un précédent commit.

**Vous ne pouvez pas merge ou commit sur la branche master**

Il faut l'autorisation d'au moins un mainteneur 

**Les branches**

Une branche par issue (ou fonctionnalité si vous préférez). 

On peut créer des sous-branches (pour le nettoyage du code, pour la correction de bug par exemple...)

Nom des branches : `type/nom` 

- feature: Ajout d’une nouvelle fonctionnalité
- bugfix: Correction d’un bug
- chore: Nettoyage du code
- experiment: Expérimentation de fonctionnalités.

Pour le nom : c'est le nom de la fonctionnalité, en **camelCase**, en anglais.




## Règles communication

Communiquez quand vous travaillez sur une tâche, ou quand vous faites un commit. 

Demandez de l'aide quand vous êtes bloqués ! On est plus fort à plusieurs :) 


