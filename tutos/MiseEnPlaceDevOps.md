# Mise en place de l'intégration continue suivant la méthode DevOps #

Cette doc est juste à titre informatif pour comprendre ce qu'est l'intégration continue et le DevOps, vous n'aurez rien à mettre en place :) 

## Qu'est-ce que le DevOps ##

Le DevOps est une méthode de travail regroupement des bonnes pratiques (dont l'intégration continue) qui permettent d'automatiser les processus.

**Pour notre projet, nous allons en mettre en place l'intégration continue**

## Qu'est ce que l'intégration continue (CI) ##

- CI = Continuous integration

**La CI permet dans un projet :**

- Réduire le temps de développement et accélérer la mise en production des fonctionnalités (Time-To-Market).
- Réduire les erreurs
- Assurer la continuitée des services d'une application

**L'intégration continue est divisée en 5 étapes :** 

- La planification des étapes de développement (Méthode Agile)
- La compilation et l'intégration du code (Build)
- Les tests du code (Test)
- La qualité du code (Quality)
- La gestion des livrables du projet (Package)

## Mise en place de l'intégration continue sur GitLab ##

Nous allons utiliser l'outil GitLab CI.

Lors de l'ajout de code sur notre repository GitLab (lors d'un push), le code va passer par 4 étapes (aussi appelées *Stage*) : Build, Test, Quality, Package. 

Pour passer à l'étape suivante, il faut que l'étape précédente soit validée. 

Si une étape n'est pas validée, le code est rejeté.

Ces 4 étapes ont un nom : **la pipeline d'intégration**.

**Build** : Compilation du code ajouté avec l'ensemble du code du projet déjà sur le GitLab pour vérifier que tout fonctionne.

**Test** : Lancement des tests déjà présent dans le projet.

**Quality** : Mesure la qualité du code qui va être ajouté. 

**Package** : Permet de déployer votre code plus facilement (mise en production du code).

## Définition de la pipeline ##

La définition de la *pipeline* se fait dans le fichier *.gitlab-ci.yml*.

La définition des étapes de la pipeline se fait avec le mot clé *stage*. (ex: Build)

Puis chaque stage est divisé en tâches qui sont définies avec le mot clé *job*. (ex: job_build)

## Avoir un affichage complet de la pipeline sous GitLab ##

Sur GitLab, aller dans le menu *CI/CL* et choisir *Pipelines*.

Sur cette page, dans la colonne *Stage* on peut voir jusqu'à quelle étape chaque ajout de code s'est arrêté. 

Si voulez voir le détail, il faut cliquer sur le check mark.

**Pour ce projet, il n'y a que le stage *Test* qui est oppérationnel pour l'instant**

Donc si votre code n'est pas accepté sur le repository Git soit vos tests ne sont plus à jour, soit il y a un bug dans votre code et il vous faudra résoudre le problème pour pouvoir ajouter votre code.