# Test en React-Native
Les test unitaires servent à vérifier l'intégrité et le bon fonctionnement du code.
> On écrit un test pour confronter une réalisation à sa spécification. Le test définit un critère d'arrêt (état ou sorties à l'issue de l'exécution) et permet de statuer sur le succès ou sur l'échec d'une vérification. Grâce à la spécification, on est en mesure de faire correspondre un état d'entrée donné à un résultat ou à une sortie. Le test permet de vérifier que la relation d'entrée / sortie donnée par la spécification est bel et bien réalisée.

### Pourquoi tester ?

  - Faciliter le débuggage car on sait quel test échoue
  - Lors de l'ajout de nouvelles fonctionnalités, on sait si l'application reste stable

# Structure

Dans le dossier `.tests/` se trouve nos fichiers de test.
Pour chaque fichier `NomDuFichier.js` on trouvera son fichier test : `NomDuFichier.test.js`

# Executer les test

Pour executer tous les test on utilise la commande: `npm test`
Pour un fichier en particulier : `npm test [Chemin]/NomDufichier.test.js`

# Rédiger un Test
**Documentation officielle :**
https://jestjs.io/docs/en/getting-started
https://jestjs.io/docs/en/tutorial-react-native

## **Tester une fonction**

```js
test('Description du test', () => {
    const UneVariable = []
    expect("coucou la team").toBe("coucou la team");
  });
```
```js
test('the best flavor is grapefruit', () => {
  expect(bestLaCroixFlavor()).toBe('grapefruit');
});
```
**Vous trouverez toutes les méthodes les plus utilisées sur cette documentation:** https://jestjs.io/docs/en/expect.html#content


## **Tester un composant** 

Jest offre la possibilité de tester la structure d'un composant. 

Il créé un "snapshot", une sauvegarde d'un composant. Lorsque qu'un lance le test d'un composant, Jest teste si la sauvegarde précédente correspond à la nouvelle. 

ex : 

On a un composant `Intro`

```js
// Intro.js
import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';

class Intro extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>
          This is a React Native snapshot test.
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    flex: 1,
    justifyContent: 'center',
  },
  instructions: {
    color: '#333333',
    marginBottom: 5,
    textAlign: 'center',
  },
  welcome: {
    fontSize: 20,
    margin: 10,
    textAlign: 'center',
  },
});

export default Intro;
```

On va ensuite créer le test : 

```js
//tests/intro-test.js
import React from 'react';
import renderer from 'react-test-renderer';
import Intro from '../components/intro';

test('renders correctly', () => {
  const tree = renderer.create(<Intro />).toJSON();
  expect(tree).toMatchSnapshot();
});
```

Si un changement est repéré, le test va échouer. 

Il faut utiliser la commande `jest -u` si le changement du composant est voulu ! 