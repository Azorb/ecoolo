# Mettre en place l'environnement de travail

## 1 Installer Node.js
C’est un moteur Javascript qui permet de traduire le code js en code machine. C’est utilisé principalement pour écrire des services côtés serveur. Expo ne pourra pas lancer notre appli sans Node.js

Téléchargement : https://nodejs.org/en/

## 2 Installer Expo 
Rien de plus simple, il suffit de suivre cette page : 
https://docs.expo.io/get-started/installation/

(Je ne vous dis pas comment faire, parce que faut prendre le temps de lire les docs 😊) 

Attention, il faut avoir installé git pour pouvoir utiliser Expo ! 

## 3 Créez un projet expo
Créez d’abord un projet dans le chemin que vous voulez. Ouvrez un terminal, positionez vous dans le dossier souhaité puis tapez la commande : 
```
expo init ecoolo
```
Cette commande va vous créer un projet react-native fonctionnant avec expo. J'expliquerai un peu plus tard l'architecture de ce projet.

Choisissez ensuite le template par défaut (blank), vous avez juste à appuyer sur Entrée



## 4 Supprimez les fichiers communs 
Certains fichiers créés par expo init sont déjà présents sur le repo git et sont communs pour tous. Quand vous allez récuperer ces fichiers sur le repo, il risque d'avoir des soucis de conflits. 

Pour éviter ça, il faut supprimer tous vos fichiers et dossier du projet à l'exception du dossier **node_module**.

**Attention !** Par défaut, expo ajoute un dossier .git au projet lors de sa création. Vous devez donc donc activer la vision des éléments masqués. 

Sur Windows, il suffit d'aller dans le menu _"Affichage"_, situé tout en haut à gauche de votre explorateur de fichier, et de cocher la case _"Eléments masqués"_

Une fois que tout est supprimé à l'exception de votre dossier node_module, on peut passer à l'étape suivante.

## 5 Initialiser le repo git

Si vous avez un compte git lab, rendez vous sur vos projets partagés. Si vous ne voyez pas le projet, c'est que j'ai mal fait mon taff, il suffira de m'envoyer un petit mp :) 

Si vous n'avez pas de compte git lab, je vous invite à le faire.

Nous allons utiliser le SSH comme protocole pour notre gitlab. Le HTTP marche aussi, mais j'ai toujours eu moins de problème d'authentification avec le SSH :) 

Vous pouvez avoir une clé SSH en suivant les tutos gitlab c'est assez simple : https://docs.gitlab.com/ee/ssh/

Récupérez le lien du clone SSH puis positionez vous dans votre dossier de projet (ecoolo)

Faite la commande 
```
git init
```
Puis la commande 
```
git remote add origin lelienducloneSSH
```
_Vous l'aurez compris il va falloir remplacer lelienducloneSSH par le vrai lien_

Une fois cela fait, vous pouvez faire un :
```
git pull origin master
```
afin d'avoir les fichiers manquant au projet ! 


## 6 Lancer l'application 

Vous pouvez maintenant lancer l'application ! 
Il suffit de lancer la commande : 
```
expo start
```
dans un terminal, situé dans le dossier de votre projet.

Un QR code va appaître, ainsi que votre console web.

Installez l'application Expo sur votre téléphone afin de pouvoir lancer l'appli.

Une fois installée, vous avez juste besoin de scanner ce QR, et vous aurez accès à l'application ! 

