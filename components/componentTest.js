import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {TextStyle} from '../styles'

export default class TestComponent extends React.Component{

    render(){
        return (
            <View>
                <Text style = {TextStyle.red}>Je suis le premier test</Text>
                <Text style = {TextStyle.size11}>Je suis le deuxième test</Text>
                <Text style = {styles.redAndSize11}>Je suis le test mélangé ! </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    redAndSize11 : {
      ...TextStyle.red,
      ...TextStyle.size11
    }
  })